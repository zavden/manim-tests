def return_random_from_word(word):
    rango = list(range(len(word)))
    rango_c = rango.copy()
    random_list = []
    for i in rango:
        num_random = random.choice(rango_c)
        rango_c.remove(num_random)
        random_list.append(num_random)
    return random_list

def return_random_direction(word):
    return [random.choice([UP,DOWN]) for _ in range(len(word))]

class NewRandom(Scene):
    def construct(self):
        text = TextMobject("Esto es una fórmula").set_width(FRAME_WIDTH-0.5)
        self.play(
            LaggedStart(*[
                    FadeInFrom(text[0][i],d)
                    # FadeIn(text[0][i],rate_func=linear)
                    for i,d in zip(return_random_from_word(text[0]),return_random_direction(text[0]))
                ],
                lag_ratio=0.07
            ),
            run_time=2.5
        )
        self.wait(3)